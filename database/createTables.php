<?php
/*
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "project2";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 
//*/

include('config.php');




// sql to create table----------//USER----------------
$sql = "CREATE TABLE User (
id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY, 
name VARCHAR(30) NOT NULL,
username VARCHAR(30) NOT NULL,
password VARCHAR(30) NOT NULL,
email VARCHAR(50),
company VARCHAR(50) NOT NULL,
field VARCHAR(50)
)";

if ($conn->query($sql) === TRUE) {
    echo "Table User created successfully";
} else {
    echo "Error creating table: " . $conn->error;
}
echo nl2br("\n");

// sql to create table---------//Project_Person------------
$sql2 = "CREATE TABLE Project_Person (
persID INT(6), 
projectID INT (6)
)";

if ($conn->query($sql2) === TRUE) {
    echo "Table Project_Person created successfully";
} else {
    echo "Error creating table: " . $conn->error;
}
echo nl2br("\n");


// sql to create table-------------//Comment-------------
$sql3 = "CREATE TABLE Comment (
msgID INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY, 
projectID INT(6),
submitter VARCHAR(30) NOT NULL,
msg LONGTEXT,
sub_time TIMESTAMP
)";

if ($conn->query($sql3) === TRUE) {
    echo "Table ReadMsg created successfully";
} else {
    echo "Error creating table: " . $conn->error;
}
echo nl2br("\n");


// sql to create table-----------//ReadMsg--------------
$sql4 = "CREATE TABLE ReadMsg (
msgID INT(6), 
userID INT(6)
)";

if ($conn->query($sql4) === TRUE) {
    echo "Table ReadMsg created successfully";
} else {
    echo "Error creating table: " . $conn->error ;
}
echo nl2br("\n");


$conn->close();
?>
<html>
    <head>
        <meta charset="windows-1252">
        <title>Creating tables</title>
    </head>
   <body>
	  <h2><a href = "runOnce.php">Back to menu</a></h2>
   </body>

    </body>
</html>