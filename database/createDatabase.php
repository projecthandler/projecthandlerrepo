<?php
$servername = "localhost";
$username = "root";
$password = "";

// Create connection
$conn = new mysqli($servername, $username, $password);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 

// Create database
$sql = "CREATE DATABASE project2";
if ($conn->query($sql) === TRUE) {
    echo "Database created successfully";
} else {
    echo "Error creating database: " . $conn->error;
}

$conn->close();
?>
<html>
    <head>
        <meta charset="windows-1252">
        <title>Creating database</title>
    </head>
   <body>
	  <h2><a href = "runOnce.php">Back to menu</a></h2>
   </body>

    </body>
</html>